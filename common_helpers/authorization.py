import jwt
import boto3
import time
import os
from common_helpers.utils import ApiGatewayException

JWT_SECRET_PARAM_NAME = os.getenv('JWT_SECRET_PARAM_NAME')


class AuthorizationToken():

    def __init__(self, token=None, user_id=None):
        self.token = token
        self.ssm_name = JWT_SECRET_PARAM_NAME
        self.secret = self._get_secret()
        self.expires_at = None
        self.user_id = user_id

    def is_token_valid(self):
        now = int(time.time())
        if self.expires_at > now:
            return True
        else:
            raise ApiGatewayException(
                status_code=403,
                error_type='TokenExpired',
                error_message='Appended token is expired'
            )

    def _get_secret(self):
        ssm_client = boto3.client('ssm')
        return ssm_client.get_parameter(Name=self.ssm_name)['Parameter']['Value']

    def decode_token(self):
        try:
            decoded_token = jwt.decode(self.token, self.secret, algorithms="HS256")
            self.expires_at = decoded_token.get('expiresAt')
            self.user_id = decoded_token.get('userId')
        except BaseException as e:
            raise ApiGatewayException(
                status_code=400,
                error_type='InvalidToken',
                error_message='Appended token is invalid'
            )

    def encode_token(self):
        self.token = jwt.encode(self._prepare_payload(), self.secret, algorithm='HS256')
        return self.token

    def _prepare_payload(self):
        return {
            'userId': self.user_id,
            'signedAt': int(time.time()),
            'expiresAt': int(time.time()) + 3600
        }



