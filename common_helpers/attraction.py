from common_helpers.attraction_constants import (
    ADD_ATTRACTION_QUERY,
    UPDATE_ATTRACTION_QUERY,
    LOAD_ATTRACTION_QUERY,
    GET_ATTRACTION_QUERY_OBJECT,
    DELETE_ATTRACTION_QUERY,
    SCHEDULE_SET_QUERY
)

from datetime import datetime, timedelta
from common_helpers.utils import LOGGER, ApiGatewayException
from common_helpers.database_helper import DatabaseHelper
import copy


class Attraction():

    def __init__(self, attraction_id=None):
        self.attraction_id = attraction_id
        self.object_id = None
        self.name = None
        self.price = None
        self.available_from = None
        self.available_to = None
        self.attraction_description = None
        self.duration_min = None
        self.break_min = None

    def map_from_result(self, result):
        self.attraction_id, self.object_id, self.name, self.price, self.available_from, self.available_to, \
        self.attraction_description, self.duration_min, self.break_min = result

    @property
    def daily_schedule(self):
        available_from = datetime.strptime(str(self.available_from), "%H:%M:%S")
        available_to = datetime.strptime(str(self.available_to), "%H:%M:%S")
        duration_min = timedelta(seconds=int(self.duration_min)*60)
        break_min = timedelta(seconds=int(self.break_min)*60)

        unit_from = available_from
        unit_to = available_from

        timesheet = []

        while unit_to + duration_min <= available_to:
            cell = {}
            unit_to = unit_from + duration_min
            cell.update(From=str(unit_from.strftime("%H:%M")), To=str(unit_to.strftime("%H:%M")))
            unit_from = unit_to + break_min
            timesheet.append(cell)

        return timesheet

    def to_json(self):
        return {
            "AttractionId": str(self.attraction_id),
            "ObjectId": str(self.object_id),
            "Name": str(self.name),
            "Price": str(self.price),
            "AvailableFrom": str(self.available_from),
            "AvailableTo": str(self.available_to),
            "Description": str(self.attraction_description),
            "Duration": str(self.duration_min),
            "Break": str(self.break_min),
        }

    def from_json(self, attraction_json):
        self.attraction_id = attraction_json.get('AttractionId', self.attraction_id)
        self.object_id = attraction_json.get('ObjectId', self.object_id)
        self.name = attraction_json.get('Name', self.name)
        self.price = attraction_json.get('Price', self.price)
        self.available_from = attraction_json('AvailableFrom', self.available_from)
        self.available_to = attraction_json('AvailableTo', self.available_to)
        self.attraction_description = attraction_json.get('Description', self.attraction_description)
        self.duration_min = attraction_json.get('Duration', self.duration_min)
        self.break_min = attraction_json.get('Break', self.break_min)

    def update(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(UPDATE_ATTRACTION_QUERY)
        self.load()
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while updating attraction in the database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def load(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(LOAD_ATTRACTION_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            result = database_helper.execute_select_query()
            if result is None:
                raise ApiGatewayException(
                    status_code=404,
                    error_type='NotFound',
                    error_message=f'There is no attraction with such id ({self.attraction_id}) in the database.'
                )
            self.map_from_result(result[0])
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while loading attraction details from the table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def delete(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(DELETE_ATTRACTION_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while deleting attraction from database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def add(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(ADD_ATTRACTION_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while adding attraction to the table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def determine_schedule(self, dates):

        time_sheet = []
        for date in dates:
            print(datetime.strptime(date, '%d-%m-%Y'))
            print(datetime.today())
            if datetime.strptime(date, '%d-%m-%Y') + timedelta(hours=24) <= datetime.today():
                time_sheet.append({
                    "Date": date,
                    "Message": 'InvalidDate'
                })
                continue

            cells = []
            for cell in self.daily_schedule:
                from_datetime = date + " " + cell.get('From')
                to_datetime = date + " " + cell.get('To')
                new_cell = dict(From=from_datetime, To=to_datetime, Available=True)
                cells.append(new_cell)

            cells = self._check_cells_availability(cells)

            time_sheet.append({
                "Date": date,
                "Cells": cells
            })
        return time_sheet

    def _check_cells_availability(self, cells):
        from_set = str()
        for cell in cells:
            from_datetime = datetime.strptime(cell.get('From'), '%d-%m-%Y %H:%M').strftime('%Y-%m-%d %H:%M')
            to_datetime = datetime.strptime(cell.get('To'), '%d-%m-%Y %H:%M').strftime('%Y-%m-%d %H:%M')
            if from_set == "":
                from_set = from_set + f'from_date=\'{from_datetime}\''
            else:
                from_set = from_set + f' or from_date=\'{from_datetime}\''
        if from_set == "":
            return cells
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(SCHEDULE_SET_QUERY)
        database_helper.set_sql_query(sql.format(attraction_id=self.attraction_id, from_set=from_set))
        results = database_helper.execute_select_query()

        for cell in cells:
            for result in results:
                if cell.get('From') == result[0].strftime('%d-%m-%Y %H:%M') or \
                        result[0] < datetime.now():
                    cell.update(Available=False)
            if datetime.strptime(cell.get('From'), '%d-%m-%Y %H:%M') < datetime.now():
                cell.update(Available=False)

        return cells









