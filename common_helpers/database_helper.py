from mysql import connector
from common_helpers.utils import ApiGatewayException, LOGGER


class DatabaseHelper:

    CONNECTION_PARAMS = {
        'host': 'trivago-app-database.c8t5x9hssnhb.eu-west-1.rds.amazonaws.com',
        'user': 'admin',
        'password': 'TrivagoPZS2020',
        'database': 'app_trivago_database'
    }

    def __init__(self, ):
        self.sql_query = None
        self._connect()

    def _connect(self):
        self.database_connect = connector.connect(**self.CONNECTION_PARAMS)

    def set_sql_query(self, sql_query):
        self.sql_query = sql_query

    def execute_select_query(self):
        if not self.database_connect.is_connected():
            self._connect()
        cursor = self.database_connect.cursor()
        LOGGER.info(f'Executing sql query {self.sql_query}')
        cursor.execute(self.sql_query)
        result = cursor.fetchall()
        self.database_connect.close()
        LOGGER.info(f'SQL Select query result {result}')
        return result

    def execute_commit_query(self, val=None):
        if not self.database_connect.is_connected():
            self._connect()
        cursor = self.database_connect.cursor()
        if val:
            LOGGER.info(f'Executing sql query {self.sql_query} with values {val}')
            cursor.execute(self.sql_query, val)
        else:
            LOGGER.info(f'Executing sql query {self.sql_query}')
            cursor.execute(self.sql_query)
        self.database_connect.commit()
        self.database_connect.close()