NEW_RESERVATION_QUERY = """
INSERT INTO Reservations(user_id, room_id, attraction_id, book_date, from_date, to_date, reservation_status) 
VALUES ('{UserId}', '{RoomId}', '{AttractionId}', '{BookDate}', '{From}', '{To}', '{ReservationStatus}')
"""

UPDATE_RESERVATION_QUERY = """
UPDATE Reservations SET user_id='{UserId}', room_id='{RoomId}', attraction_id='{AttractionId}', from_date='{From}', 
to_date='{To}', reservation_status='{ReservationStatus}'
WHERE reservation_id='{ReservationId}'
"""

RESERVATION_DETAILS_QUERY = """
SELECT * FROM Reservations
WHERE reservation_id='{ReservationId}'
"""

DELETE_RESERVATION_QUERY = """
DELETE FROM Reservations where reservation_id='{ReservationId}'
"""