from common_helpers.utils import LOGGER, ApiGatewayException
from common_helpers.database_helper import DatabaseHelper
from common_helpers.object_constants import (
    UPDATE_OBJECT_QUERY,
    OBJECT_DETAILS_QUERY,
    DELETE_OBJECT_QUERY,
    NEW_OBJECT_QUERY
)
import copy


class Object():

    def __init__(self, object_id=None):
        self.object_id = object_id
        self.object_name = None
        self.country = None
        self.city = None
        self.street = None
        self.house_number = None
        self.zip_code = None,
        self.standard = None,
        self.description = None

    def map_from_result(self, result):
        self.object_id, self.object_name, self.country, self.city, self.street, self.house_number, \
        self.zip_code, self.standard, self.description = result

    def to_json(self):
        return {
            "ObjectId": str(self.object_id),
            "ObjectName": str(self.object_name),
            "Country": str(self.country),
            "City": str(self.city),
            "Street": str(self.street),
            "HouseNumber": str(self.house_number),
            "PostalCode": str(self.zip_code),
            "Standard": str(self.standard),
            "Description": str(self.description)
        }

    def from_json(self, object_json):
        self.object_id = object_json.get('ObjectId', self.object_id),
        self.object_name = object_json.get('ObjectName', self.object_name),
        self.country = object_json.get('Country', self.country),
        self.city = object_json.get('City', self.city),
        self.street = object_json.get('Street', self.street),
        self.house_number = object_json.get('HouseNumber', self.house_number),
        self.zip_code = object_json.get('PostalCode', self.zip_code),
        self.standard = object_json.get('Standard', self.standard),
        self.description = object_json.get('Description', self.standard)

    def update(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(UPDATE_OBJECT_QUERY)
        self.load()
        try:
            database_helper.set_sql_query(sql.format(self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while updating object in the database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def load(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(OBJECT_DETAILS_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            result = database_helper.execute_select_query()
            if result is None:
                raise ApiGatewayException(
                    status_code=404,
                    error_type='NotFound',
                    error_message=f'There is no attraction with such id ({self.object_id}) in the database.'
                )
            self.map_from_result(result[0])
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while loading object details from the table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def delete(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(DELETE_OBJECT_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while deleting object from database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def add(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(NEW_OBJECT_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while adding attraction to the table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )




