USER_INFO_SELECT = """
SELECT Users.user_id, first_name, last_name, telephone_number, birth_date, 
country, city, zip_code, sex, business_role, C.email_address, 
C.user_password, account_status
FROM Users
JOIN Credentials C on Users.user_id = C.user_id
WHERE {where_statement}"""

USER_ID_SELECT = """
SELECT user_id from Credentials where email_address='{Email}'
"""

WHERE_STATEMENTS = {
    'user_id': "Users.user_id='{user_id}'",
    'email': "C.email_address='{email}'"
}

INSERT_USER_DETAILS = """
INSERT INTO Users (user_id, first_name, last_name, telephone_number, 
birth_date, country, city, zip_code, sex, business_role) 
VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
"""

INSERT_USER_CREDENTIALS = """
INSERT INTO Credentials (email_address, user_password, account_status) 
VALUES (%s, %s, %s)
"""

UPDATE_USER_DETAILS = """
UPDATE Users set first_name='{}', last_name='{}', telephone_number='{}', 
birth_date='{}', country='{}', city='{}', zip_code='{}', sex='{}', business_role='{}'
WHERE user_id='{}'
"""

UPDATE_USER_CREDENTIALS = """
UPDATE Credentials set email_address='{}', user_password='{}', account_status='{}' 
WHERE user_id='{}'
"""

DELETE_USER_DETAILS = """
DELETE FROM Users WHERE user_id='{}'
"""

DELETE_USER_CREDENTIALS = """
DELETE FROM Credentials WHERE user_id='{}'
"""