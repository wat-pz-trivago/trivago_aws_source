NEW_OBJECT_QUERY = """
INSERT INTO Objects (object_name, country, city, street, house_number, zip_code, standard, decription) 
VALUES ('{ObjectName}', '{Country}', '{City}', '{Street}', '{HouseNumber}', '{PostalCode}', '{Standard}', '{Description}')
"""

UPDATE_OBJECT_QUERY = """
UPDATE Objects SET object_name='{ObjectName}', country='{Country}', city='{City}', street='{Street}', 
house_number='{HouseNumber}', zip_code='{PostalCode}', standard='{Standard}', decription='{Description}'
WHERE object_id='{ObjectId}'
"""

OBJECT_DETAILS_QUERY = """
SELECT * FROM Objects
WHERE object_id='{ObjectId}'
"""

DELETE_OBJECT_QUERY = """
DELETE FROM Objects WHERE object_id='{ObjectId}'
"""