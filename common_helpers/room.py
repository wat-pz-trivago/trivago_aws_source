from common_helpers.room_constants import UPDATE_ROOM_DETAILS, NEW_ROOM_QUERY, ROOM_DETAILS_QUERY, DELETE_ROOM_QUERY
from common_helpers.utils import LOGGER, ApiGatewayException
from common_helpers.database_helper import DatabaseHelper
import copy


class Room():

    def __init__(self, room_id=None):
        self.room_id = room_id
        self.object_id = None
        self.room_number = None
        self.price = None
        self.bed_configuration = None
        self.room_description = None

    def map_from_result(self, result):
        LOGGER.info(result)
        self.room_id, self.object_id, self.room_number, self.price, self.bed_configuration, self.room_description = result[slice(6)]
        if len(result) > 6:
            return result[slice(6, len(result))]

    def update(self):
        database = DatabaseHelper()
        sql = copy.deepcopy(UPDATE_ROOM_DETAILS)
        try:
            database.set_sql_query(sql.format(**self.to_json()))
            database.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while updating rooms table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def to_json(self):
        return {
            'RoomId': str(self.room_id),
            'ObjectId': str(self.object_id),
            'RoomNumber': str(self.room_number),
            'Price': str(self.price),
            'BedConfiguration': str(self.bed_configuration),
            'GuestsAmount': str(self.guests_amount),
            'Description': str(self.room_description)
        }

    def from_json(self, room_json):
        self.room_id = room_json.get('RoomId', self.room_id)
        self.object_id = room_json.get('ObjectId', self.object_id)
        self.room_number = room_json.get('RoomNumber', self.room_number)
        self.price = room_json.get('Price', self.price)
        self.bed_configuration = room_json.get('BedConfiguration', self.bed_configuration)
        self.room_description = room_json.get('Description', self.room_description)

    @property
    def guests_amount(self):
        beds = self.bed_configuration.split(',')
        max_guests = 0
        for bed in beds:
            max_guests += int(bed)
        return max_guests

    def add(self):
        sql = copy.deepcopy(NEW_ROOM_QUERY)
        database = DatabaseHelper()
        try:
            database.set_sql_query(sql.format(**self.to_json()))
            database.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while adding room to the table')
        raise ApiGatewayException(
            status_code=502,
            error_type='BadGateway',
            error_message='Database connector error, please try again later.'
        )

    def load(self):
        sql = copy.deepcopy(ROOM_DETAILS_QUERY)
        database = DatabaseHelper()
        try:
            database.set_sql_query(sql.format(RoomId=self.room_id))
            result = database.execute_select_query()
            if not result:
                raise ApiGatewayException(
                    status_code=404,
                    error_type='NotFound',
                    error_message=f'There is no room with given room id ({self.room_id})'
                )
            self.map_from_result(result[0])
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while adding room to the table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def delete(self):
        self.load()
        sql = copy.deepcopy(DELETE_ROOM_QUERY)
        database = DatabaseHelper()
        try:
            database.set_sql_query(sql.format(RoomId=self.room_id))
            database.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while deleting room from the table')
        raise ApiGatewayException(
            status_code=502,
            error_type='BadGateway',
            error_message='Database connector error, please try again later.'
        )




