from common_helpers.database_helper import DatabaseHelper
from common_helpers.utils import ApiGatewayException, LOGGER
from mysql.connector import Error
from common_helpers.user_constants import (
    USER_INFO_SELECT,
    WHERE_STATEMENTS,
    INSERT_USER_DETAILS,
    INSERT_USER_CREDENTIALS,
    UPDATE_USER_DETAILS,
    UPDATE_USER_CREDENTIALS,
    UPDATE_USER_DETAILS,
    UPDATE_USER_CREDENTIALS,
    DELETE_USER_DETAILS,
    DELETE_USER_CREDENTIALS,
    USER_ID_SELECT
)
import copy


class User:
    user_id = None

    def __init__(self, user_id=None, email=None):
        self.user_id = user_id
        self.name = None
        self.last_name = None
        self.email = email
        self.telephone_number = None
        self.birth_date = None
        self.country = None
        self.city = None
        self.zip_code = None
        self.sex = None
        self.business_role = None
        self.password = None
        self.account_status = None

    def to_json(self):
        return {
            "UserId": str(self.user_id),
            "Email": str(self.email),
            "FirstName": str(self.name),
            "LastName": str(self.last_name),
            "TelephoneNumber": str(self.telephone_number),
            "BirthDate": str(self.birth_date),
            "Country": str(self.country),
            "City": str(self.city),
            "PostalCode": str(self.zip_code),
            "Sex": str(self.sex),
            "BusinessRole": str(self.business_role)
        }

    def fill_user(self, event):
        self.user_id = event.get('UserId', self.user_id)
        self.name = event.get('FirstName', self.name)
        self.last_name = event.get('LastName', self.last_name)
        self.email = event.get('Email', self.email)
        self.telephone_number = event.get('PhoneNumber', self.telephone_number)
        self.birth_date = event.get('BirthDate', self.birth_date)
        self.country = event.get('Country', self.country)
        self.city = event.get('City', self.city)
        self.zip_code = event.get('PostalCode', self.zip_code)
        self.sex = event.get('Sex', self.sex)
        self.business_role = event.get('Type', self.business_role)
        self.password = event.get('Password', self.password)

    def determine_user_id(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(USER_ID_SELECT)
        database_helper.set_sql_query(sql.format(Email=self.email))
        result = database_helper.execute_select_query()
        if result:
            self.user_id = str(result[0][0])
            return True
        else:
            raise ApiGatewayException(
                status_code=404,
                error_type='NotFound',
                error_message='User with given user_id does not exists.'
            )

    def load(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(USER_INFO_SELECT)
        if self.user_id:
            LOGGER.info(f'Getting user details basing on user_id: {self.user_id}.')
            where_statement = copy.deepcopy(WHERE_STATEMENTS['user_id'])
            where_statement = where_statement.format(user_id=self.user_id)
        else:
            LOGGER.info(f'Getting user details basing on email: {self.email}.')
            where_statement = copy.deepcopy(WHERE_STATEMENTS['email'])
            where_statement = where_statement.format(email=self.email)
        database_helper.set_sql_query(sql.format(where_statement=where_statement))
        result = database_helper.execute_select_query()
        if result:
            self.map_values(result)
        else:
            raise ApiGatewayException(
                status_code=404,
                error_type='NotFound',
                error_message='User with given user_id or email does not exists.'
            )

    def exists(self):
        try:
            self.load()
            return True
        except ApiGatewayException:
            return False

    def check_password(self, password):
        if self.password != password:
            raise ApiGatewayException(
                status_code=403,
                error_type='Unauthorized',
                error_message=f'Wrong password'
            )

    def is_active(self):
        if self.account_status == 'NOT ACTIVATED':
            raise ApiGatewayException(
                status_code=403,
                error_type='Unauthorized',
                error_message=f'Account with given address email ({self.email}) is not activated'
            )
        if self.account_status == 'BLOCKED':
            raise ApiGatewayException(
                status_code=403,
                error_type='Unauthorized',
                error_message=f'Account with given address email ({self.email}) is blocked'
            )
        return self.account_status == 'ACTIVATE'

    def map_values(self, result):
        self.user_id, self.name, self.last_name, self.telephone_number, self.birth_date, self.country,\
        self.city, self.zip_code, self.sex, self.business_role, self.email, self.password, self.account_status = result[0]

    def write_to_user_details_database(self):
        database_helper = DatabaseHelper()
        sql = INSERT_USER_DETAILS
        val = (self.user_id, self.name, self.last_name, self.telephone_number,
               self.birth_date, self.country, self.city, self.zip_code,
               self.sex, self.business_role)
        database_helper.set_sql_query(sql)
        try:
            database_helper.execute_commit_query(val=val)
        except Error as e:
            LOGGER.exception('Exception occurred while executing commit query on database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def write_to_user_credentials_database(self):
        database_helper = DatabaseHelper()
        sql = INSERT_USER_CREDENTIALS
        val = (self.email, self.password, self.account_status)
        database_helper.set_sql_query(sql)
        try:
            database_helper.execute_commit_query(val=val)
        except Error as e:
            LOGGER.exception('Exception occurred while executing commit query on database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def update_user_details_database(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(UPDATE_USER_DETAILS)
        val = (self.name, self.last_name, self.telephone_number,
               self.birth_date, self.country, self.city, self.zip_code,
               self.sex, self.business_role, self.user_id)
        sql = sql.format(*val)
        database_helper.set_sql_query(sql)
        try:
            database_helper.execute_commit_query()
        except Error as e:
            LOGGER.exception('Exception occurred while executing commit query on database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def update_user_credentials_database(self):
        database_helper = DatabaseHelper()
        sql = UPDATE_USER_CREDENTIALS
        val = (self.email, self.password, self.account_status, self.user_id)
        sql = sql.format(*val)
        database_helper.set_sql_query(sql)
        try:
            database_helper.execute_commit_query()
        except Error as e:
            LOGGER.exception('Exception occurred while executing commit query on database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def delete_user_account(self):
        LOGGER.info(f'Deleting user with parameters {self.to_json()}')
        database_helper = DatabaseHelper()
        sql_credentials = copy.deepcopy(DELETE_USER_CREDENTIALS).format(self.user_id)
        sql_details = copy.deepcopy(DELETE_USER_DETAILS).format(self.user_id)
        database_helper.set_sql_query(sql_details)
        try:
            LOGGER.info(f'Deleting from credentials table')
            database_helper.execute_commit_query()
            database_helper.set_sql_query(sql_credentials)
            LOGGER.info(f'Deleting from user details table')
            database_helper.execute_commit_query()
        except Error as e:
            LOGGER.exception('Exception occurred while executing commit query on database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )








