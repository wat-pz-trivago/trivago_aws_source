from common_helpers.utils import LOGGER, ApiGatewayException
from common_helpers.database_helper import DatabaseHelper
from common_helpers.attraction import Attraction
from common_helpers.room import Room

from common_helpers.reservation_constants import (
    NEW_RESERVATION_QUERY,
    UPDATE_RESERVATION_QUERY,
    RESERVATION_DETAILS_QUERY,
    DELETE_RESERVATION_QUERY
)

import copy


class Reservation():

    def __init__(self, reservation_id=None):
        self.reservation_id = reservation_id
        self.user_id = None
        self.room_id = None
        self.attraction_id = None
        self.book_date = None
        self.from_date = None
        self.to_date = None
        self.reservation_status = None

    @property
    def total_price(self):
        if self.attraction_id:
            attraction = Attraction(attraction_id=self.attraction_id)
            attraction.load()
            cost = attraction.price

        elif self.room_id:
            room = Room(room_id=self.room_id)
            room.load()
            cost = room.price
        else:
            return None
        total_cost = int((self.to_date - self.from_date).seconds / 60) * cost
        return float(total_cost)

    def map_from_result(self, result):
        self.reservation_id, self.user_id, self.room_id, self.attraction_id, self.book_date, self.from_date, \
        self.to_date, self.reservation_status = result
        pass

    def edit_reservation_details(self):
        pass

    def to_json(self):
        return {
            "ReservationId": str(self.reservation_id),
            "UserId": str(self.user_id),
            "RoomId": str(self.room_id),
            "BookDate": str(self.book_date),
            "From": str(self.from_date),
            "To": str(self.to_date),
            "ReservationStatus": str(self.reservation_status),
            "TotalCost": str(self.total_price)
        }

    def from_json(self, reservation_json):
        self.reservation_id = reservation_json.get('ReservationId', self.reservation_id),
        self.room_id = reservation_json.get('RoomId', self.room_id),
        self.user_id = reservation_json.get('UserId', self.user_id),
        self.book_date = reservation_json.get('BookDate', self.book_date),
        self.from_date = reservation_json.get('From', self.from_date),
        self.to_date = reservation_json.get('To', self.to_date)
        self.reservation_status = reservation_json.get('ReservationStatus', self.reservation_status)

    def add(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(NEW_RESERVATION_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while adding reservation to the table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def load(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(RESERVATION_DETAILS_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            print(database_helper.sql_query)
            result = database_helper.execute_select_query()
            if result is None:
                raise ApiGatewayException(
                    status_code=404,
                    error_type='NotFound',
                    error_message='There is no reservation with such id in the database.'
                )
            self.map_from_result(result[0])
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while loading reservation details from the table')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def delete(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(DELETE_RESERVATION_QUERY)
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while deleting reservation from database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )

    def update(self):
        database_helper = DatabaseHelper()
        sql = copy.deepcopy(UPDATE_RESERVATION_QUERY)
        self.load()
        try:
            database_helper.set_sql_query(sql.format(**self.to_json()))
            database_helper.execute_commit_query()
        except Exception as e:
            LOGGER.exception(f'{type(e).__name__} occurred while updating reservation in the database')
            raise ApiGatewayException(
                status_code=502,
                error_type='BadGateway',
                error_message='Database connector error, please try again later.'
            )




