ADD_ATTRACTION_QUERY = """
INSERT INTO Attractions(object_id, price, attraction_description, open_from, open_to, name, duration_min, break_min)
VALUES ('{ObjectId}','{Price}','{Description}','{AvailableFrom}','{AvailableTo}','{Name}','{Duration}','{Break}')
"""

UPDATE_ATTRACTION_QUERY = """
UPDATE Attractions SET object_id='{ObjectId}', price='{Price}', attraction_description='{Description}',
open_from='{AvailableFrom}',open_to='{AvailableTo}',name='{Name}',duration_min='{Duration}',break_min='{Break}'
WHERE attraction_id='{AttractionId}'
"""

DELETE_ATTRACTION_QUERY = """
DELETE FROM Attractions WHERE attraction_id='{AttractionId}'
"""

LOAD_ATTRACTION_QUERY = """
SELECT * FROM Attractions WHERE attraction_id='{AttractionId}'
"""

GET_ATTRACTION_QUERY_OBJECT = """
SELECT * FROM Attractions WHERE object_id='{ObjectId}'
"""

SCHEDULE_SET_QUERY = """
SELECT from_date FROM Reservations
WHERE attraction_id = '{attraction_id}' and reservation_status='ACTIVE' and ({from_set})
"""