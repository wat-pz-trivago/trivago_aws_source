import json
from json import JSONDecodeError
import logging

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def prepare_response(result):
    response = dict()
    if result:
        if 'message' in result:
            message = {
                'Message': result.get('message')
            }
            response.update(body=json.dumps(message))

        if 'body' in result:
            response.update(body=json.dumps(result.get('body')))

        if 'headers' in result:
            response.update(headers=response.get('headers'))
        response.update(statusCode=result.get('status_code', 200))
    else:
        message = {
            'Message': 'Internal server error'
        }
        response.update(statusCode=500, body=json.dumps(message))

    return response


def handle_exception(exception):

    status_code = 500
    error_message = 'Unexpected server error, please contact backend support.'

    if isinstance(exception, ApiGatewayException):
        return {
            'status_code': exception.status_code,
            'message': exception.error_message
        }
    else:
        return {
            'status_code': status_code,
            'message': error_message
        }


class ApiGatewayException(Exception):
    def __init__(self, status_code, error_type, error_message):
        self.status_code = status_code
        self.error_type = error_type
        self.error_message = error_message
        super()




