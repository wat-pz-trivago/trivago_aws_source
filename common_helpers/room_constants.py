UPDATE_ROOM_DETAILS = """
UPDATE Rooms SET room_number='{RoomNumber}', price='{Price}', bed_configuration='{BedConfiguration}',
room_description='{Description}' WHERE room_id='{RoomId}'"""

NEW_ROOM_QUERY = """
INSERT INTO Rooms(object_id, room_number, price, bed_configuration, room_description) 
VALUES ('{ObjectId}', '{RoomNumber}', '{Price}', '{BedConfiguration}', '{Details}') 
"""

ROOM_DETAILS_QUERY = """
SELECT * FROM Rooms
WHERE room_id='{RoomId}'
"""

DELETE_ROOM_QUERY = """
DELETE FROM Rooms where room_id='{RoomId}'
"""




