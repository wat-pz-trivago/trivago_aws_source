from setuptools import find_packages, setup


setup(
    name='common_helpers',
    packages=find_packages(),
    version='0.0.1',
    description='Helpers for PZ project',
    author='Paweł Kierski',
    license='WAT',
)