set -x
echo 'Starting deployment'
# shellcheck disable=SC2034
ROOT_PATH="/home/pawel/PZ/trivago_aws_source"
MAIN_PATH="lambda-layers/python/lib/python3.8/site-packages"
if [ ! -d $MAIN_PATH ]
then
  mkdir -p lambda-layers/python/lib/python3.8/site-packages
  pip3 install objectpath --target $MAIN_PATH
fi
pip3 install . --target $MAIN_PATH --upgrade
pip3 install -r common_helpers/requirements.txt --target $MAIN_PATH
cd $ROOT_PATH/lambda-layers || echo "No directory"
chmod -R 777 python
chmod -R 777 data
zip -r common-helpers-layer.zip data python
cd $ROOT_PATH || echo "No directory"
aws s3 cp lambda-layers s3://trivago-app-layers/ --grants full=uri=http://acs.amazonaws.com/groups/global/AllUsers --recursive
sam package --template-file template.yaml --output-template-file deploy.yaml --s3-bucket app-backend-source
sam deploy --template-file deploy.yaml --stack-name app-trivago --capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_NAMED_IAM
rm deploy.yaml