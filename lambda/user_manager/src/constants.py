from mysql import connector

EMAIL_SUBJECT = ' {first_name} twoje konto zostało pomyślnie utworzone!'
EMAIL_BODY = '''
Drogi {first_name},

Gratulujemy założenia konta w naszej aplikacji! 
W celu dokończenia rejestracji prosimy potwierdzić adres e-mail klikając w poniższy link.
{url}

Jeśli nie zakładałeś konta w aplikacji TRIVAGO prosimy zignoruj tą wiadomość.

Zespół TRIVAGO
'''

CONNECTION_PARAMS = {
    'host': 'aws-trivago-database.c8t5x9hssnhb.eu-west-1.rds.amazonaws.com',
    'user': 'admin',
    'password': 'TrivagoPZS2020',
    'database': 'app_trivago_database'
}

USERS_TABLE_COLUMNS = '(user_id, first_name, last_name, telephone_number, birth_date, country, city, post_code, sex, business_role)'

VERIFY_ENDPOINT = 'https://7f6kavfkr2.execute-api.eu-west-1.amazonaws.com/v1/register/{verification}'

