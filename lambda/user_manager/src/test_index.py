from test_constants import REQUEST, HAPPY_RESPONSE, REGISTER_REQUEST, UNI_USER_REQUEST
from common_helpers.user import User
from index import lambda_handler
import json
import copy


def test_user_exists_happy():
    result = lambda_handler(REQUEST, None)
    assert result == HAPPY_RESPONSE


def test_user_register_happy():
    result = lambda_handler(REGISTER_REQUEST, None)
    assert result == HAPPY_RESPONSE


def test_update_user_info():
    request = copy.deepcopy(UNI_USER_REQUEST)
    user = User(user_id=19)
    user.load()
    user_json = user.to_json()
    user.name = 'Grzegorz'
    request.update(body=json.dumps(user_json))
    result = lambda_handler(request, None)
    assert result == HAPPY_RESPONSE


def test_get_user_info():
    request = copy.deepcopy(UNI_USER_REQUEST)
    result = lambda_handler(request, None)
    assert result == HAPPY_RESPONSE
