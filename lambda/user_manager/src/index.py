import boto3
import logging
import base64
import copy
import json

from botocore.exceptions import ClientError
from common_helpers.user import User
from common_helpers.utils import prepare_response, handle_exception, ApiGatewayException

from constants import (
    EMAIL_BODY,
    EMAIL_SUBJECT,
    VERIFY_ENDPOINT,
)


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):

    LOGGER.info(f'Received event {event}')
    request = event

    request_body = None
    path_params = None
    result = None

    try:
        if request.get('body') != 'None' and not request.get('body') is None:
            request_body = json.loads(request.get('body'))
        if request.get('pathParameters'):
            path_params = request.get('pathParameters')

        if '/register' == request.get('resource'):
            LOGGER.info('Start of serving register request.')
            result = register_user(request_body)
        if '/user/{user_id}' == request.get('resource'):
            user = User(user_id=path_params['user_id'])
            if not user.exists():
                raise ApiGatewayException(
                    status_code=404,
                    error_type='NotFound',
                    error_message='User with given user_id does not exists'
                )
            if request.get('httpMethod') == 'DELETE':
                LOGGER.info('Start of serving delete request.')
                result = handle_user_delete_request(user)
            if request.get('httpMethod') == 'POST':
                LOGGER.info('Start of serving user data update request.')
                result = handle_user_post_request(user, request_body)
            if request.get('httpMethod') == 'GET':
                LOGGER.info('Start of serving get user details request.')
                result = handle_user_get_request(user)
    except KeyError as e:
        LOGGER.exception('KeyError exception occurred, check request payload')
        result = {
            'status_code': 400,
            'message': 'Wrong schema'
        }
    except BaseException as e:
        LOGGER.exception(f'{type(e).__name__} occurred')
        result = handle_exception(e)

    finally:
        response = prepare_response(result)
        LOGGER.info(f'Sending response to the application {response}')
        return prepare_response(result)


def handle_user_get_request(user):
    user_json = user.to_json()
    return {
        'body': user_json
    }


def handle_user_delete_request(user):
    user.delete_user_account()
    return {
        'message': f'User with id {user.user_id} account was successfully deleted from database'
    }


def handle_user_post_request(user, request_body):
    user.fill_user(request_body)
    user.update_user_details_database()
    user.write_to_user_credentials_database()
    return {
        'body': user.to_json()
    }


def handle_register_get_request(request_body):
    user = User()
    user.fill_user(request_body)
    if user.exists():
        LOGGER.error(f'User exists')
        raise ApiGatewayException(
            status_code=400,
            error_type='BadRequest',
            error_message='User with given e-mail address already exists in database'
        )
    else:
        return {
            'message': 'Email address is unused'
        }


def register_user(register_details):
    user = User()
    user.fill_user(register_details)
    if user.exists():
        LOGGER.error(f'User exists')
        raise ApiGatewayException(
            status_code=400,
            error_type='BadRequest',
            error_message='User with given e-mail address already exists in database'
        )
    user.account_status = 'NOT ACTIVATED'
    user.write_to_user_credentials_database()
    user.determine_user_id()
    user.write_to_user_details_database()
    send_verify_email(register_details)
    return {
        'message': 'Account was created successfully'
    }


def send_verify_email(user_data):

    encoded = base64.urlsafe_b64encode(user_data.get('Email').encode())
    email_encoded = str(encoded).split('\'')[1]
    formatted_endpoint = copy.deepcopy(VERIFY_ENDPOINT)

    ses_client = boto3.client('ses')
    subject = EMAIL_SUBJECT.format(first_name=user_data['FirstName'])
    body = EMAIL_BODY.format(first_name=user_data['FirstName'],
                             url=formatted_endpoint.format(verification=email_encoded))

    try:
        response = ses_client.send_email(
            Source='trivago.pz@gmail.com',
            Destination={
                'ToAddresses': [user_data.get('Email')]
            },
            Message={
                'Subject': {
                    'Data': subject
                },
                'Body': {
                    'Text': {
                        'Data': body,
                    }
                }
            }
        )
        LOGGER.info(f"An verification email was sent to {user_data.get('Email')} successfully")
        return True
    except ClientError as e:
        LOGGER.exception(f"Verification message could not be sent on given email address {user_data.get('Email')}")
        LOGGER.info(f"Performing rollback on database")
        user = User(email=user_data.get('Email'))
        user.determine_user_id()
        user.delete_user_account()
        LOGGER.info("Rollback completed")
        raise ApiGatewayException(
            status_code=502,
            error_type='BadGateway',
            error_message='There was an unexpected error. Please try again later.'
        )






