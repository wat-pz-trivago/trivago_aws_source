import json
import logging
import time
import boto3
import os
import traceback
from common_helpers.authorization import AuthorizationToken
from botocore.exceptions import ClientError


S3_BUCKET = os.getenv('USER_PASSWORDS_TABLE')
S3_CLIENT = boto3.client('s3')

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def lambda_handler(event, context):

    LOGGER.info(f'Received event {event}')
    token = event.get('headers').get('Authorization')

    try:
        if event.get('resource') == "/content/{file_type}/{content_type}/{content_index}":
            if event.get('httpMethod') == "POST":
                insert_photo_into_bucket(event.get('pathParameters'), event.get('body'), token)
                return prepare_response(200, {"Message": "Item inserted successfully"})
            elif event.get('httpMethod') == "GET":
                items_list = list_items_in_bucket(event.get('pathParameters'))
                return prepare_response(200, {"ObjectsList": items_list})
        if event.get('resource') == "/content/{file_type}/{content_type}/{content_index}/{file_name}":
            if event.get("httpMethod") == "GET":
                item = get_item_from_bucket(event.get('pathParameters'))
                return prepare_response(200, {"BinaryFile": item})
            if event.get("httpMethod") == "DELETE":
                delete_item_from_bucket(event.get('pathParameters'))
                return prepare_response(200, {"Message": "Item deleted successfully"})
    except ClientError as e:
        traceback.print_exc()
        LOGGER.error('Operation failed')
        return prepare_response(500, 'Item cannot be inserted. Try again.')


def insert_photo_into_bucket(parameters, body, token):

    content = parameters['content_type']
    index = parameters['content_index']
    file_type = parameters['file_type']

    file_name = f'file_{int(time.time())}.{file_type}'
    key = f'{content}/{index}/{file_name}'

    response = S3_CLIENT.put_object(
        Body=body,
        Bucket=S3_BUCKET,
        Key=key
    )

    return None


def list_items_in_bucket(parameters):

    content = parameters['content_type']
    index = parameters['content_index']

    response = S3_CLIENT.list_objects(
        Bucket=S3_BUCKET,
        Prefix=f'{content}/{index}'
    )
    files_names = []
    contents = response['Contents']
    for content in contents:
        files_names.append(content['Key'].split('/')[-1])
    return files_names


def get_item_from_bucket(parameters):

    file_name = parameters['file_name']
    content = parameters['content_type']
    index = parameters['content_index']

    key = f'{content}/{index}/{file_name}'
    response = S3_CLIENT.get_object(
        Bucket=S3_BUCKET,
        Key=key
    )

    object = response['Body']
    file = object.read()
    file_str = str(file)

    return file_str


def delete_item_from_bucket(parameters):
    pass


def prepare_response(status_code, message):
    if status_code != 200:
        message = {
            'Message': message
        }

    return {
        'statusCode': status_code,
        'body': json.dumps(message)
    }
