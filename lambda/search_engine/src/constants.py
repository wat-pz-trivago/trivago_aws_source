ROOMS_QUERY = """SELECT Rooms.*
FROM Rooms
INNER JOIN Objects ON Rooms.object_id = Objects.object_id
WHERE Rooms.object_id IN (
    SELECT object_id
    FROM Objects
    {where_statement}
) AND (
    SELECT Reservations.room_id
    FROM Reservations
    WHERE room_id = Rooms.room_id
    AND ({start_date} BETWEEN from_date AND to_date
    OR {end_date} BETWEEN from_date AND to_date)
) IS NULL;"""


OBJECTS_QUERY = """SELECT DISTINCT Rooms.*, Objects.*
FROM Rooms
INNER JOIN Objects ON Rooms.object_id = Objects.object_id
WHERE Rooms.object_id IN (
    SELECT object_id
    FROM Objects
    {where_statement}
) AND (
    SELECT Reservations.room_id
    FROM Reservations
    WHERE room_id = Rooms.room_id
    AND ({start_date} BETWEEN from_date AND to_date
    OR {end_date} BETWEEN from_date AND to_date)
) IS NULL;"""

ALL_OBJECTS = """
SELECT country, city, object_name from Objects
"""