from mysql import connector
import logging
import json
from datetime import datetime
from common_helpers.utils import LOGGER, handle_exception, prepare_response
from common_helpers.database_helper import DatabaseHelper
from common_helpers.room import Room
from common_helpers.object import Object

from constants import (
    ROOMS_QUERY,
    OBJECTS_QUERY,
    ALL_OBJECTS
)


def lambda_handler(event, context):
    LOGGER.info(f'Received api event {event}')

    result = None

    try:
        if event['queryStringParameters']:
            event_body = json.loads(event.get('body'))
            if 'Object' in event_body:
                LOGGER.info('Listing available rooms in specific hotel')
                result = filter_rooms(event['queryStringParameters'], event_body)
            else:
                LOGGER.info('Listing hotels with available rooms')
                result = filter_objects(event['queryStringParameters'], event_body)
        else:
            LOGGER.info('Listing all hotels and localizations')
            result = list_all_hotels()
    except BaseException as e:
        LOGGER.exception('Exception occurred while searching')
        result = handle_exception(exception=e)
    finally:
        if not result:
            result = {
                'status-code': 404,
                'message': 'There is no items that meet the conditions'
            }
        response = prepare_response(result=result)
        LOGGER.info(f'Sending response {response}')
        return response


def filter_objects(filter_params, filter_body):

    database_helper = DatabaseHelper()

    where = f"WHERE country=\'{filter_params.get('country')}\'"

    from_datetime = datetime.strptime(filter_body.get('From') + " 11:00:00", '%d.%m.%Y %H:%M:%S').strftime("%Y-%m-%d %H:%M:%S")
    to_datetime = datetime.strptime(filter_body.get('To') + " 10:00:00", '%d.%m.%Y %H:%M:%S').strftime("%Y-%m-%d %H:%M:%S")

    if 'city' in filter_params:
        where = where + f" AND city=\'{filter_params.get('city')}\'"
    if 'standard' in filter_params:
        where = where + f" AND standard=\'{filter_params.get('standard')}\'"
    sql = OBJECTS_QUERY.format(where_statement=where,
                               start_date=f"\'{from_datetime}\'",
                               end_date=f" \'{to_datetime}\'")

    database_helper.set_sql_query(sql)
    results = database_helper.execute_select_query()

    hotels = []
    for result in results:
        append = True
        room = Room()
        hotel = Object()
        rest = room.map_from_result(result)
        hotel.map_from_result(rest)
        LOGGER.info(f"ROOM GUESTS {room.guests_amount} FILTER GUESTS {filter_params.get('persons')}")
        if not filter_params.get('persons') or int(room.guests_amount) == int(filter_params.get('persons')):
            for obj in hotels:
                if obj.object_id == room.object_id:
                    append = False
            if append:
                hotels.append(hotel)

    objects = []

    for hotel in hotels:
        objects.append(hotel.to_json())

    return {"body": {"Objects": objects}}


def filter_rooms(filter_params, filter_body):

    database_helper = DatabaseHelper()

    where = f"WHERE country=\'{filter_params.get('country')}\' AND object_name=\'{filter_body.get('Object')}\'"

    from_datetime = datetime.strptime(filter_body.get('From') + " 11:00:00", '%d.%m.%Y %H:%M:%S').strftime("%Y-%m-%d %H:%M:%S")
    to_datetime = datetime.strptime(filter_body.get('To') + " 10:00:00", '%d.%m.%Y %H:%M:%S').strftime("%Y-%m-%d %H:%M:%S")

    if 'City' in filter_params:
        where = where + f" AND city=\'{filter_params.get('city')}\'"
    if 'Standard' in filter_params:
        where = where + f" AND standard=\'{filter_params.get('standard')}\'"
    sql = ROOMS_QUERY.format(where_statement=where,
                             start_date=f"\'{from_datetime}\'",
                             end_date=f" \'{to_datetime}\'")

    database_helper.set_sql_query(sql)
    results = database_helper.execute_select_query()
    rooms = []

    for result in results:
        room = Room()
        room.map_from_result(result)
        if not filter_params.get('persons') or int(room.guests_amount) == int(filter_params.get('persons')):
            rooms.append(room.to_json())

    return {"body": {"Rooms": rooms}}


def list_all_hotels():
    database_helper = DatabaseHelper()
    LOGGER.debug('Preparing json with all objects')
    database_helper.set_sql_query(ALL_OBJECTS)
    results = database_helper.execute_select_query()

    all_objects = {}

    for result in results:
        if not result[0] in all_objects:
            country = {
                result[0]: {}
            }
            all_objects.update(**country)
        if not result[1] in all_objects[result[0]]:
            city = {
                result[1]: []
            }
            all_objects[result[0]].update(**city)
        all_objects[result[0]][result[1]].append(result[2])

    return {"body": all_objects}



