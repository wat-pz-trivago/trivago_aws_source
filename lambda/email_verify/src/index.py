
import base64
import os
import re
from common_helpers.user import User
from common_helpers.utils import handle_exception, prepare_response, LOGGER
from constants import ACTIVATE_PAGE

JWT_SECRET_SMM = os.getenv('JWT_SECRET_PARAM_NAME')


def lambda_handler(event, context):

    LOGGER.info(f'Received verification e-mail event {event}')
    encoded_email = event['pathParameters'].get('verification')
    email_decoded = base64.urlsafe_b64decode(encoded_email).decode()
    LOGGER.info(f'Activation for e-mail {email_decoded} requested')
    if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email_decoded):
        try:
            user = User(email=email_decoded)
            user.load()
            user.account_status = 'ACTIVE'
            user.update_user_credentials_database()

            result = {
                'body': ACTIVATE_PAGE,
                'headers': {
                    'Content-Type': 'text/html'
                }
            }
            LOGGER.info(f'User {user.to_json()} account activated')

        except KeyError as e:
            LOGGER.exception('KeyError exception occurred, check request payload')
            result = {
                'status_code': 400,
                'message': 'Wrong schema'
            }
        except BaseException as e:
            LOGGER.exception(f'{type(e).__name__} occurred')
            result = handle_exception(e)

        response = prepare_response(result)
        LOGGER.info(f'Sending response to the client {response}')
        return response