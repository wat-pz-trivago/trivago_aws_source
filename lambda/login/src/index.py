from common_helpers.user import User
from common_helpers.utils import ApiGatewayException, LOGGER, handle_exception, prepare_response
from common_helpers.authorization import AuthorizationToken


def lambda_handler(event, context):

    LOGGER.info(f'Received event from API GW {event}')

    result = None
    email, password = extract_params(event)
    user = User(email=email)

    try:
        LOGGER.info('Checking if account exists.')
        user.load()
        LOGGER.info('Checking if passwords match.')
        user.check_password(password)
        LOGGER.info('Checking if account is active.')
        user.is_active()
        LOGGER.info('Generating JWT token')
        token_helper = AuthorizationToken(user_id=user.user_id)
        jwt_token = token_helper.encode_token()

        result = {
            'body': dict(Token=jwt_token, UserData=user.to_json())
        }
    except BaseException as e:
        LOGGER.exception(f'{type(e).__name__} occurred')
        result = handle_exception(e)

    finally:
        response = prepare_response(result)
        LOGGER.info(f'Sending response to the application {response}')
        return prepare_response(result)


def extract_params(event):
    request_parameters = event['queryStringParameters']
    return request_parameters['email'], request_parameters['password']
