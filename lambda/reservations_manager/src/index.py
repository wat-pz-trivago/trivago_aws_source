from mysql import connector
import json
from json import JSONDecodeError
from copy import deepcopy
from authorization import AuthorizationToken, InvalidToken, TokenExpired
from botocore.exceptions import ClientError
import traceback
from datetime import datetime, timedelta
import boto3

from constants import (
    CONNECTION_PARAMS,
    FILTERED_ROOM_RESERVATIONS,
    GENERAL_RESERVATION_FILTERS,
    ROOM_FILTERS,
    ATTRACTION_FILTERS,
    FILTERED_ATTRACTION_RESERVATIONS,
    LOGGER,
    MAKE_ROOM_RESERVATION_SQL,
    MAKE_ATTRACTION_RESERVATION_SQL,
    CHECK_RESERVATION_SQL,
    USER_SELECT_SQL,
    OBJECT_DETAILS_ROOM_SQL,
    OBJECT_DETAILS_ATTRACTION_SQL,
    NOTIFICATION_EMAIL_BODY_ROOM,
    NOTIFICATION_EMAIL_BODY,
    USER_FILTERS,
    RESERVATION_UPDATE,
    RESERVATION_DATE_SQL,
    DELETE_RESERVATION,
    RESERVATION_CHANGE_MESSAGE,
    RESERVATION_DELETE_MESSAGE
)


def lambda_handler(event, context):
    LOGGER.info(f'Received event {event}')
    token = event.get('headers').get('Authorization')

    code = 500
    payload = "Internal server error."

    database_connect = connector.connect(**CONNECTION_PARAMS)

    try:
        auth_token = AuthorizationToken(token)
        auth_token.is_token_valid()
    except ClientError as e:
        LOGGER.error("Secret could not be get from SSM params")
        return prepare_response(500, 'Internal server error')
    except TokenExpired as e:
        return prepare_response(403, 'Token expired')
    except InvalidToken as e:
        traceback.print_exc()
        return prepare_response(403, 'Invalid token')

    if event['httpMethod'] == 'GET':
        code, payload = handle_get_request(event, database_connect, auth_token)
    elif event['httpMethod'] == 'POST':
        code, payload = handle_post_request(event, database_connect, auth_token)

    return prepare_response(code, payload)


def handle_get_request(event, database_connect, auth_token):
    sql = deepcopy(FILTERED_ROOM_RESERVATIONS)

    if event['queryStringParameters'] != 'None':
        query_params = event['queryStringParameters']
        sql = sql + prepare_where_statement(query_params, ROOM_FILTERS, GENERAL_RESERVATION_FILTERS, USER_FILTERS)

    cursor = database_connect.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()

    reservations = []
    for reservation in results:

        parameters = {
            "RoomId": reservation[2]
        }
        reservation_dict = {
            "ReservationId": str(reservation[0]),
            "UserId": str(reservation[1]),
            "EmailAddress": str(reservation[16]),
            "RoomId": str(reservation[2]),
            "RoomNumber": str(reservation[11]),
            "ObjectId": str(reservation[9]),
            "ObjectName": str(reservation[20]),
            "BookDate": str(reservation[4]),
            "FromDate": str(reservation[5]),
            "ToDate": str(reservation[6]),
            "ReservationStatus": str(reservation[7]),
            "UnitPrice": str(reservation[12]),
            "TotalPrice": str(reservation[8])
        }

        reservations.append(reservation_dict)

    query_params = event['queryStringParameters']
    sql = deepcopy(FILTERED_ATTRACTION_RESERVATIONS)
    sql = sql + prepare_where_statement(query_params, ATTRACTION_FILTERS, GENERAL_RESERVATION_FILTERS, USER_FILTERS)

    cursor.execute(sql)
    results = cursor.fetchall()

    for reservation in results:
        reservation_dict = {
            "ReservationId": str(reservation[0]),
            "UserId": str(reservation[1]),
            "EmailAddress": str(reservation[14]),
            "ObjectId": str(reservation[9]),
            "ObjectName": str(reservation[18]),
            "AttractionId": str(reservation[3]),
            "BookDate": str(reservation[4]),
            "FromDate": str(reservation[5]),
            "ToDate": str(reservation[6]),
            "ReservationStatus": str(reservation[7]),
            "UnitPrice": str(11),
            "TotalPrice": str(reservation[8]),
            "AttractionDescription": str(reservation[12])
        }

        reservations.append(reservation_dict)

    LOGGER.info(reservations)
    return 200, {"Reservations": reservations}


def determine_ids(reservation_id, database_connect):

    sql = f"Select room_id, attraction_id from Reservations WHERE reservation_id={reservation_id}"
    cursor = database_connect.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()

    return results[0][0], results[0][1]


def delete_period_exceeded(reservation_id, database_connect):
    sql = RESERVATION_DATE_SQL
    sql = sql.format(reservation_id=reservation_id)

    cursor = database_connect.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()
    start_date = datetime.strptime(results[0][0].date, 'Y-d-m HH:MM:SS')
    now = datetime.now()

    return not (start_date - now).days > 14


def handle_delete_request(event, database_connect, token):
    parameters = parse_to_json(event)
    if 'Delete' in parameters:
        if delete_period_exceeded(parameters.get('ReservationId'), database_connect):
            return 403, 'Period that reservation cancel is possible is exceeded for this reservation'
        else:
            sql = DELETE_RESERVATION.format(parameters.get('ReservationId'))
            cursor = database_connect.cursor()
            cursor.execute(sql)
            database_connect.commit()
        return 200, {'Message': 'Reservation deleted'}


def handle_post_request(event, database_connect, token):
    parameters = parse_to_json(event)
    if 'ReservationId' in parameters:
        sql = deepcopy(RESERVATION_UPDATE)

        from_datetime = datetime.strptime(parameters.get('From') + " 11:00:00", '%d.%m.%Y %H:%M:%S')
        to_datetime = datetime.strptime(parameters.get('To') + " 10:00:00", '%d.%m.%Y %H:%M:%S')

        room_id, attraction_id = determine_ids(parameters.get('ReservationId'), database_connect)
        if not is_reservation_possible(database_connect, from_datetime, to_datetime, room_id, attraction_id):
            return 400, 'There is already reservation in given period, change it and try again'

        parameters_dict = {
            "set_statement": f"start_date='{from_datetime}', end_date='{to_datetime}'",
            "reservation_id": parameters.get('ReservationId')
        }

        sql = sql.format(**parameters_dict)
        cursor = database_connect.cursor()
        cursor.execute(sql)
        database_connect.commit()

        return 200, {'Message': 'Reservation changed successfully'}

    else:

        from_datetime = datetime.strptime(parameters.get('From') + " 11:00:00", '%d.%m.%Y %H:%M:%S')
        to_datetime = datetime.strptime(parameters.get('To') + " 10:00:00", '%d.%m.%Y %H:%M:%S')

        if not is_reservation_possible(database_connect, from_datetime, to_datetime,
                                       parameters.get('RoomId'), parameters.get('AttractionId')):
            return 400, 'There is already reservation in given period, change it and try again'

        parameters_dict = dict(
            UserId=token.user_id,
            BookDate=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            From=from_datetime.strftime(
                "%Y-%m-%d %H:%M:%S"),
            To=to_datetime.strftime(
                "%Y-%m-%d %H:%M:%S"),
            ReservationStatus='ACTIVE'
        )

        if parameters.get('RoomId'):
            sql = deepcopy(MAKE_ROOM_RESERVATION_SQL)
            parameters_dict.update(RoomId=parameters.get('RoomId'))
        else:
            sql = deepcopy(MAKE_ATTRACTION_RESERVATION_SQL)
            parameters_dict.update(AttractionId=parameters.get('AttractionId'))

        total_cost = determine_total_cost(parameters_dict, database_connect, from_datetime, to_datetime)
        parameters_dict.update(TotalCost=total_cost)

        sql = sql.format(**parameters_dict)

        cursor = database_connect.cursor()
        cursor.execute(sql)
        database_connect.commit()

        send_notification_email(database_connect, token.user_id, parameters_dict)

        return 200, parameters_dict


def send_notification_email(database_connect, user_id, reservation_details):
    ses_client = boto3.client('ses')
    cursor = database_connect.cursor()
    message = ""
    if reservation_details.get('RoomId'):
        sql = OBJECT_DETAILS_ROOM_SQL
        cursor.execute(sql.format(room_id=reservation_details.get('RoomId')))
        message = NOTIFICATION_EMAIL_BODY_ROOM
    elif reservation_details.get('AttractionId'):
        sql = OBJECT_DETAILS_ATTRACTION_SQL
        cursor.execute(sql.format(attraction_id=reservation_details.get('AttractionId')))
        message = NOTIFICATION_EMAIL_BODY

    hotel_details = cursor.fetchall()

    sql = USER_SELECT_SQL
    cursor.execute(sql.format(user_id=user_id))
    user_details = cursor.fetchall()
    user_details=user_details[0]
    user_details = dict(
        name=user_details[1],
        second_name=user_details[2],
        e_mail=user_details[11]
    )
    hotel_details = hotel_details[0]
    hotel_details = dict(
        hotel=hotel_details[1],
        country=hotel_details[2],
        city=hotel_details[3],
        street=hotel_details[4],
        house_number=hotel_details[5],
        zip_code=hotel_details[6],
        standard=hotel_details[7],
    )

    message_parameters = dict(
        name=user_details['name'],
        second_name=user_details['second_name'],
        hotel=hotel_details['hotel'],
        stars='*' * int(hotel_details['standard']),
        book_date=reservation_details['BookDate'],
        from_date=reservation_details['From'],
        to_date=reservation_details['To'],
        total_price=reservation_details['TotalCost'],
        street=hotel_details['street'],
        house_number=hotel_details['house_number'],
        zip_code=hotel_details['zip_code'],
        city=hotel_details['city'],
        country=hotel_details['country']
    )

    body = message.format(**message_parameters)

    try:
        response = ses_client.send_email(
            Source='trivago.pz@gmail.com',
            Destination={
                'ToAddresses': [user_details['e_mail']]
            },
            Message={
                'Subject': {
                    'Data': f"Rezerwacja z dnia {datetime.today().strftime('%d.%m.%Y')}"
                },
                'Body': {
                    'Text': {
                        'Data': body,
                    }
                }
            }
        )
        LOGGER.info(f"An verification email was sent to {user_details['e_mail']} successfully")
        return True
    except ClientError as e:
        traceback.print_exc()
        LOGGER.error(f"Verification message could not be sent to {user_details['e_mail']}")
        return False


def is_reservation_possible(database_connect, from_date, to_date, room_id, attraction_id):

    sql = deepcopy(CHECK_RESERVATION_SQL)

    parameters = {
        "where_statement": f"room_id={room_id}" if room_id else f"attraction_id={attraction_id}",
        "start_date": from_date,
        "end_date": to_date
    }

    sql = sql.format(**parameters)

    cursor = database_connect.cursor()
    cursor.execute(sql)
    results = cursor.fetchall()

    return False if results else True


def determine_total_cost(parameters_dict, database_connect, from_datetime, to_datetime):

    if 'AttractionId' in parameters_dict:
        sql = f"SELECT price FROM Attractions WHERE attraction_id={parameters_dict['AttractionId']}"
        cursor = database_connect.cursor()
        cursor.execute(sql)
        results = cursor.fetchall()
        cost = results[0][0]
        total_cost = int((to_datetime - from_datetime).seconds/60) * cost

    else:
        sql = f"SELECT price FROM Rooms WHERE room_id={parameters_dict['RoomId']}"
        cursor = database_connect.cursor()
        cursor.execute(sql)
        results = cursor.fetchall()
        cost = results[0][0]
        total_cost = ((to_datetime - from_datetime).days + 1) * cost

    return float(total_cost)


def prepare_where_statement(parameters, detail_filters, reservation_filters, user_filters):
    where_statement = ""

    for detail_filter in detail_filters:
        if detail_filters == ROOM_FILTERS:
            if detail_filter not in parameters:
                continue
            if where_statement == "":
                where_statement = f" WHERE Rooms.{detail_filter}=\'{parameters[detail_filter]}\'"
            else:
                where_statement = where_statement + f" AND Rooms.{detail_filter}=\'{parameters[detail_filter]}\'"
        else:
            if detail_filter not in parameters:
                continue
            if where_statement == "":
                where_statement = f" WHERE Attractions.{detail_filter}=\'{parameters[detail_filter]}\'"
            else:
                where_statement = where_statement + f" AND Attractions.{detail_filter}=\'{parameters[detail_filter]}\'"

    for reservation_filter in reservation_filters:
        if reservation_filter not in parameters:
            continue
        if where_statement == "":
            where_statement = f" WHERE {reservation_filter}=\'{parameters[reservation_filter]}\'"
        else:
            where_statement = where_statement + f" AND {reservation_filter}=\'{parameters[reservation_filter]}\'"

    for user_filter in user_filters:
        if user_filter not in parameters:
            continue
        if where_statement == "":
            where_statement = f" WHERE C.{user_filter}=\'{parameters[user_filter]}\'"
        else:
            where_statement = where_statement + f" AND C.{user_filter}=\'{parameters[user_filter]}\'"

    return where_statement


def prepare_response(status_code, message):
    if status_code != 200:
        message = {
            'Message': message
        }

    LOGGER.info(f'Message sent to the client app {message}')

    return {
        'statusCode': status_code,
        'body': json.dumps(message)
    }


def send_notification(notofication_type, user_details, reservation_details, object_details, total_price):

    fill_dict = dict(
        name=user_details[0],
        second_name=user_details[1],
        reservation_id=reservation_details[0],
        book_date=reservation_details[1],
        from_date=reservation_details[2],
        to_date=reservation_details[3],
        total_price=total_price,
        hotel=object_details[0],
        street=object_details[1],
        house_number=object_details[2],
        zip_code=object_details[3],
        country=object_details[4]
    )

    if notofication_type == 'DELETE':
        sql = deepcopy(RESERVATION_DELETE_MESSAGE)

    elif notofication_type == 'CHANGE':
        sql = deepcopy(RESERVATION_CHANGE_MESSAGE)
    else:
        return False

    sql.format(fill_dict)


def parse_to_json(event):
    return json.loads(event['body'])
