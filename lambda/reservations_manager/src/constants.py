import logging

CONNECTION_PARAMS = {
    'host': 'aws-trivago-database.c8t5x9hssnhb.eu-west-1.rds.amazonaws.com',
    'user': 'admin',
    'password': 'TrivagoPZS2020',
    'database': 'app_trivago_database'
}

DELETE_RESERVATION = """
DELETE FROM Reservations WHERE reservation_id='{reservation_id}'
"""

RESERVATION_DATE_SQL = """
SELECT start_date, end_date
FROM Reservations
WHERE reservation_id = '{reservation_id}'
"""

CHECK_RESERVATION_SQL = """
SELECT *
FROM Reservations
WHERE {where_statement}
AND ('{start_date}' BETWEEN start_date AND end_date
OR '{end_date}' BETWEEN start_date AND end_date)"""

FILTERED_ROOM_RESERVATIONS = """
SELECT * 
FROM Reservations
INNER JOIN Rooms ON Rooms.room_id = Reservations.room_id
INNER JOIN Credentials C on Reservations.user_id = C.user_id
INNER JOIN Objects O on Rooms.object_id = O.object_id
"""

FILTERED_ATTRACTION_RESERVATIONS = """
SELECT * 
FROM Reservations
INNER JOIN Attractions ON Attractions.attraction_id = Reservations.attraction_id
INNER JOIN Credentials C on Reservations.user_id = C.user_id
INNER JOIN Objects O on Attractions.object_id = O.object_id
"""

MAKE_ROOM_RESERVATION_SQL = """
INSERT INTO Reservations (user_id, room_id, book_date, start_date, end_date, reservation_status) VALUES
('{UserId}', '{RoomId}', '{BookDate}', '{From}', '{To}', '{ReservationStatus}')
"""

USER_SELECT_SQL = """
SELECT U.*, Credentials.* FROM Credentials
INNER JOIN Users U on Credentials.user_id = U.user_id
WHERE Credentials.user_id='{user_id}'
"""

RESERVATION_UPDATE = """
UPDATE Reservations
SET {set_statement}
WHERE reservation_id = {reservation_id}
"""

OBJECT_DETAILS_ROOM_SQL = """
SELECT * FROM Objects
INNER JOIN Rooms R on Objects.object_id = R.object_id
WHERE room_id='{room_id}'
"""

OBJECT_DETAILS_ATTRACTION_SQL = """
SELECT * FROM Objects
INNER JOIN Attractions A on Objects.object_id = A.object_id
WHERE attraction_id='{attraction_id}'
"""

MAKE_ATTRACTION_RESERVATION_SQL = """
INSERT INTO Reservations (user_id, attraction_id, book_date, start_date, end_date, reservation_status, total_price) VALUES
('{UserId}', '{AttractionId}', '{BookDate}', '{From}', '{To}', '{ReservationStatus}', '{TotalPrice}')
"""

GENERAL_RESERVATION_FILTERS = ['reservation_id', 'user_id', 'room_id', 'attraction_id', 'reservation_status']
USER_FILTERS = ['email_address']
ROOM_FILTERS = ['object_id', 'room_number', 'country']
ATTRACTION_FILTERS = ['object_id']

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

NOTIFICATION_EMAIL_BODY_ROOM = """
Drogi {name} {second_name},

Gratuluacje! Dokonałeś rezerwacji w hotelu {hotel} {stars}. Poniżej znajdziesz szczegóły dokonanej rezerwacji.
Data złożenia rezerwacji: {book_date}
Początek zakwaterowania: {from_date}
Data i godzina wymelodwania: {to_date}
Do zapłaty: {total_price}

Adres: 
{hotel}, 
Ul. {street} {house_number}
{zip_code}, {city}
{country}

Więcej informacji uzyskasz w recepcji hotelu w dniu rezerwacji!

Do zobaczenia!
Zespół Trivago
"""

NOTIFICATION_EMAIL_BODY = """
Drogi {name} {second_name},

Gratuluacje! Dokonałeś rezerwacji w hotelu {hotel}. 
Poniżej znajdziesz szczegóły Twojej rezerwacji.

Data złożenia rezerwacji: {book_date}
Początek rezerwacji: {from_date}
Koniec rezerwacji: {to_date}
Do zapłąty: {total_price}

Adres: 
{hotel}, 
{street} {house_number}
{zip_code}, {city}
{country}

Więcej informacji uzyskasz w recepcji hotelu w dniu rezerwacji!

Do zobaczenia!
Zespół Trivago
"""


RESERVATION_CHANGE_MESSAGE = """
Drogi {name} {second_name},

Gratuluacje! Dokonałeś zmiany rezerwacji numer {reservation_id}.
Poniżej znajdziesz szczegóły rezerwacji.

Data złożenia rezerwacji: {book_date}
Początek rezerwacji: {from_date}
Koniec rezerwacji: {to_date}
Do zapłąty: {total_price}

Adres: 
{hotel}, 
{street} {house_number}
{zip_code}, {city}
{country}

Więcej informacji uzyskasz w recepcji hotelu w dniu rezerwacji!

Z poważaniem, 
Zespół Trivago
"""


RESERVATION_DELETE_MESSAGE = """
Drogi {name} {second_name},

Z przykrością informujemy, że dokonałeś anulowania rezerwacji numer {reservation_id}. 
Poniżej znajdziesz szczegóły anulowanej rezerwacji. 

Data złożenia rezerwacji: {book_date}
Początek rezerwacji: {from_date}
Koniec rezerwacji: {to_date}
Do zapłąty: {total_price}

Adres: 
{hotel}, 
{street} {house_number}
{zip_code}, {city}
{country}

Z poważaniem, 
Zespół Trivago
"""