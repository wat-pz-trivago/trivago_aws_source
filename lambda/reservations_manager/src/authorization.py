import jwt
import boto3
import time
from datetime import timedelta
import os
from constants import LOGGER

# JWT_SECRET_PARAM_NAME = os.getenv('JWT_SECRET_PARAM_NAME')


class AuthorizationToken():

    def __init__(self, token):
        self.token = token
        # self.ssm_name = JWT_SECRET_PARAM_NAME
        # self.secret = self._get_secret()
        self.secret = 'SECRET!@#11'
        self.expires_at = None
        self.user_id = None

    def is_token_valid(self):
        now = int(time.time())
        if self.expires_at > now:
            LOGGER.info(f"Token valid")
            LOGGER.info(f"Token will expire in {int((self.expires_at - now) / 60)} minutes")
            return True
        else:
            raise TokenExpired

    def _get_secret(self):
        ssm_client = boto3.client('ssm')
        return ssm_client.get_parameter(Name=self.ssm_name)['Parameter']['Value']

    def _decode_token(self, token):
        try:
            self.token = token.split(" ")[1]
            decoded_token = jwt.decode(token, self.secret, algorithms="HS256")
            LOGGER.info(f"Decoded token: {decoded_token}")
            self.expires_at = decoded_token.get('expiresAt')
            self.user_id = decoded_token.get('userId')
        except BaseException as e:
            LOGGER.info('Token is invalid')
            raise InvalidToken

    def _encode_token(self):
        self.token = jwt.encode(self._prepare_payload(), self.secret, algorithm='HS256')
        return self.token

    def _prepare_payload(self):
        return {
            'userId': self.user_id,
            'signedAt': int(time.time()),
            'expiresAt': int(time.time()) + 3600
        }


class TokenExpired(Exception):
    def __init__(self):
        super()


class InvalidToken(Exception):
    def __init__(self):
        super()


