from common_helpers.user import User
from common_helpers.utils import ApiGatewayException, LOGGER, handle_exception, prepare_response
from common_helpers.authorization import AuthorizationToken
from common_helpers.room import Room
from common_helpers.attraction import Attraction
from common_helpers.reservation import Reservation
from common_helpers.database_helper import DatabaseHelper
from constants import GET_ALL_ATTRACTIONS
import json
import copy


def lambda_handler(event, context):

    LOGGER.info(f'Received event from API GW {event}')
    result = None

    try:
        if event.get('httpMethod') == 'POST':
            LOGGER.info('Start serving POST request')
            request_body = json.loads(event.get('body'))
            attraction_id = request_body.get('AttractionId')
            dates = request_body.get('Dates')
            LOGGER.info('Parameters extracted')
            if attraction_id is None or dates is None:
                raise ApiGatewayException(
                    status_code=400,
                    error_type='BadRequest',
                    error_message='Wrong input'
                )
            attraction = Attraction(attraction_id=attraction_id)
            attraction.load()
            schedule = attraction.determine_schedule(dates)

            result = {
                'body': schedule
            }

        if event.get('httpMethod') == 'GET':
            reservation_id = event['queryStringParameters'].get('reservation_id')

            reservation = Reservation(reservation_id=reservation_id)
            reservation.load()
            room_id = reservation.room_id

            room = Room(room_id=room_id)
            room.load()
            object_id = room.object_id

            database_helper = DatabaseHelper()
            sql = copy.deepcopy(GET_ALL_ATTRACTIONS)
            database_helper.set_sql_query(sql.format(object_id=object_id))
            results = database_helper.execute_select_query()

            attractions = []
            for result in results:
                tmp_attraction = Attraction()
                tmp_attraction.map_from_result(result)
                attractions.append(tmp_attraction.to_json())

            result = {
                "body": {'Attractions' : attractions}
            }

    except BaseException as e:
        LOGGER.exception(f'{type(e).__name__} occurred')
        result = handle_exception(e)
    finally:
        response = prepare_response(result)
        LOGGER.info(f'Sending response to the application {response}')
        return prepare_response(result)


